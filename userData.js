//This file is our sort of pseudo database that we are using.
module.exports = {
  users: [
    {
      username: "Darth Vader",
      password: "evil-overlord",
      role: ""
    },
    {
      username: "Stormtrooper",
      password: "thosewerethedroids",
      role: ""
    },
    {
      username: "Palpatine",
      password: "midichlorians",
      role: "admin"

    }
  ]
}
