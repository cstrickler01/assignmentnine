#Assignment 9: Authentication via DB
###12 April, 2019
###By: Chase Strickler

## Description
This project follows the following tutorial to create an authentication page with a database: https://codeburst.io/june-27-2017-62b9b5416a7f

The purpose of this project is to build an application that properly implements user authentication, and
can correctly differentiate between an admin user and a standard user.

## Dependencies
These are listed in package.json. Simply type npm install

## How to Use
Once you have the dependencies installed, from the command line navigate to the Assignment9/assignmentnine/ directory and type node index.js . The command line should then say "Started express application." Then, on your browser, navigate to localhost:3000/login to use the application. Login information can be found in /userData.js

## References
For references on how to install dependencies, see the following:
mustache: https://github.com/janl/mustache.js
express: https://expressjs.com/en/starter/installing.html
mustache-express: npm install mustache-express
GracefulShutdownManager: npm i -S @moebius/http-graceful-shutdown

cd documents/Assignment9/assignmentnine
