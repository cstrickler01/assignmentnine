/*This is the main application for our authenticating web application*/
const GracefulShutdownManager = require('@moebius/http-graceful-shutdown').GracefulShutdownManager;
var bcrypt = require('bcrypt');
const express = require('express');
const parseurl = require('parseurl');
const bodyParser = require('body-parser');
const session = require('express-session');
const mustacheExpress = require('mustache-express');
const data = require ('./userData.js');
const data2 = require ('./data.js');
const app = express();
const server =  app.listen(3000);
const shutdownManager = new GracefulShutdownManager(server)


var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/usersdb";

//How to query a database
//MongoClient.connect(url, function(err, db) {
//   if (err) throw err;
//   var dbo = db.db("usersdb");
//   var query = {user : "vader"};
//   var result = dbo.collection('usercollection').find(query).toArray(function(err, result) {
//     if (err) throw err;
//     console.log(result);
//     db.close();
//   });
// });

app.engine('mustache', mustacheExpress());
app.set('views', './views');
app.set('view engine', 'mustache');
app.use(express.static('./public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//Actually not sure what this is or what it does.
app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true
}));

app.use(function(req, res, next){
  var views = req.session.views;

  if(!views) {
    views = req.session.views = {};
  }

  // get url path
  var pathname = parseurl(req).pathname;

  // count views
  views[pathname] = (views[pathname] || 0) + 1
  next();
})

//Function used to authenticate a user
function authenticate(req, username, password, role){
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("usersdb");

    dbo.collection('usercollection').findOne({user:username, pwd:password}, function(err,doc){
      if(err) throw err;

      if(doc) {
        console.log("Found: " + username + " pass=" + password);
        req.session.authenticated = true;
      }
      else{
        console.log("Not found: " + username);
        req.session.authenticated = false;
      }
    });
  });

  // var authenticatedUser = data.users.find(function (user){
  //   if(username === user.username && password === user.password && role === user.role){
  //     req.session.authenticated = true;
  //     console.log('User & Password Authenticated');
  //   } else{
  //     return false
  //   }
  // });
  console.log(req.session);
  return req.session;
}

//Function used to check admin
function isAdmin(req, username, password, role){
  var admin = data.users.find(function (user){
    if(username === user.username && password === user.password && role === user.role && "admin" === user.role){
      req.session.authenticated = true;
      console.log('Admin found');
      return true;
    } else{
      return false
    }
  });
  return admin;
}

app.get('/', function(req, res){
  res.redirect('/login');
})

app.get('/login', function(req, res){
  res.render('index');
});

//Sort of the main function, called from index.mustache
app.post('/', function(req, res){
  var username = req.body.username;
  var password = req.body.password;
  var role = req.body.role;
  authenticate(req, username, password, role);
  //Check if the user was authenticated
  if(req.session && req.session.authenticated){
    //Check if the user is an administrator
    if(isAdmin(req, username, password, role)){
      console.log("Rendering admin page...")
        res.render('admin', {users: data2.users});
    }
    else{
      console.log("Rendering basic user page...")
        res.render('welcome', { users: data2.users});
    }
  }
  else {
    res.redirect('/');
  }
})
